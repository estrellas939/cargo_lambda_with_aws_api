use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response, IntoResponse};
use serde_json::Value;
use std::collections::HashMap;

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Try to parse JSON data from the request body
    let body = event.body();
    let numbers: Vec<i32> = match serde_json::from_slice(body) {
        Ok(nums) => nums,
        Err(_) => {
            return Ok(Response::builder()
                .status(400)
                .body("Invalid JSON input".into())
                .expect("Failed to render response"))
        }
    };

    // Use HashMap to calculate the frequency of each number
    let mut frequencies = HashMap::new();
    for &number in &numbers {
        *frequencies.entry(number).or_insert(0) += 1;
    }

    // Find the most frequent number
    let mut max_frequency = 0;
    let mut result = Vec::new();
    for (&number, &freq) in frequencies.iter() {
        if freq > max_frequency {
            max_frequency = freq;
            result.clear();
            result.push(number);
        } else if freq == max_frequency {
            result.push(number);
        }
    }

    // Construct a response based on the results
    let response_body = if result.len() == 1 {
        serde_json::json!({ "most_frequent": result[0] }).to_string()
    } else {
        serde_json::json!({ "message": "no such value due to same frequent" }).to_string()
    };

    Ok(Response::builder()
        .status(200)
        .header("Content-Type", "application/json")
        .body(response_body.into())
        .expect("Failed to render response"))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
