# Cargo_Lambda_with_AWS_API

## Part I. Build Cargo Lambda Function

### 1. Install Cargo Lambda

You can also use `PyPI` to install Cargo Lambda on any system that has Python 3 installed:
```
pip3 install cargo-lambda
```

### 2. Create a new project

The `new` subcommand will help you create a new project with a default template. When that's done, change into the new directory:

```
cargo lambda new new-lambda-project \
    && cd new-lambda-project
```

### 3. Build Function

`src/main.rs` is the excute code file.

This function can get the array from the body part when we input a json file, and return the number with the highest frequency in the array. If there are multiple numbers with the same frequency, a Message `no such value due to same frequent` will be returned.

### 4. Configuration

Write configuration info into `Cargo.toml` file.

### 5. Serve the function locally for testing

Run the Lambda emulator built in with the `watch` subcommand:

```
cargo lambda watch
```

### 6. Build test file

Cause our Rust function needs a json file as the input, we need to create a new json file in the right format to test.

`event.json` is the test file:

```
{
  "body": "[1,2,3,2]",
  "httpMethod": "POST",
  "headers": {
    "Content-Type": "application/json"
  },
  "path": "/your/path",
  "queryStringParameters": null,
  "requestContext": {
    "identity": {
      "sourceIp": "127.0.0.1"
    },
    "httpMethod": "POST",
    "path": "/your/path"
  }
}
```
We can modify the array after `"body":` to see different results.

### 7. Test function

The `invoke` subcommand can send JSON payloads to the function running locally. Payloads are deserialized into strongly typed Rust structs, and the invoke call will fail if the payload doesn't have the right shape.

```
cargo lambda invoke --data-file event.json
```

and we should see the correct output from the terminal:

```
{"statusCode":200,"headers":{"content-type":"application/json"},"multiValueHeaders":{"content-type":["application/json"]},"body":"{\"most_frequent\":2}","isBase64Encoded":false}
```

`"body":"{\"most_frequent\":2}"` shows the result.

### 8. Build the function to deploy it on AWS Lambda

Use the `build` subcommand to compile your function for Linux systems:

```
cargo lambda build --release
```

### 9. Deploy the function on AWS Lambda

Use the `deploy` subcommand to upload your function to AWS Lambda. This subcommand requires AWS credentials in your system.

```
cargo lambda deploy
```

In my case, I used the more deatiled command:

```
cargo lambda deploy --iam-role arn:aws:iam::568937614068:role/cargo-lambda-role-11f55518-6167-4e36-8783-c2bd14e1fb79
```

And the termain showed the function was deployed successfully!

```
🔍 function arn: arn:aws:lambda:us-east-1:568937614068:function:most_frequent_number
```

### 10. View the function from AWS Lambda pannel

Log in the AWS main page and search `Lambda` subpage. You should see the function deployed under `Lambda function(s)` section.

Click the function and copy its `Function ARN` from the right.

![image](https://gitlab.com/estrellas939/cargo_lambda_with_aws_api/-/raw/main/images/lambda-main.png?inline=false)

## Part II. Build AWS API Gateway

### 11. Build a new AWS Rest API Gateway

Search `API Gateway` and click. From the top right corner click `Create API` and select `REST API` on the following page.

![image](https://gitlab.com/estrellas939/cargo_lambda_with_aws_api/-/raw/main/images/build-api.png?inline=false)

Give a name.

![image](https://gitlab.com/estrellas939/cargo_lambda_with_aws_api/-/raw/main/images/build-rest-api.png?inline=false)

### 12. Create Method

From the right click `Create method`. Select method type as `POST` and integration type as `Lambda function`. Be sure to check the region and copy paste the right ARN from `Lambda` pannel perviously. So that the function can be triggered by the API Gateway.

![image](https://gitlab.com/estrellas939/cargo_lambda_with_aws_api/-/raw/main/images/build-method.png?inline=false)

### 13. Deploy API

Click `Deploy API` at the top right, and select `*New stage*`, give a name, then click `deploy`.

![image](https://gitlab.com/estrellas939/cargo_lambda_with_aws_api/-/raw/main/images/deploy-api.png?inline=false)

Back to the `Lambda` pannel, you should see the lambda function now is connected to a API indicator.
Copy the API endpoint URL for next step.

![image](https://gitlab.com/estrellas939/cargo_lambda_with_aws_api/-/raw/main/images/lambda-main.png?inline=false)

## Part III. Test API

### 14. Use Terminal to Test API

We use `curl` to test the connection to API:

```
curl -X POST https://s0lm4rjkxh.execute-api.us-east-1.amazonaws.com/dev/ \
-H "Content-Type: application/json" \
--data-binary "@event.json"
```

And we should see the same result as the local test:

```
{"statusCode":200,"headers":{"content-type":"application/json"},"multiValueHeaders":{"content-type":["application/json"]},"body":"{\"most_frequent\":2}","isBase64Encoded":false}
```

### 15. Use AWS Lambda to Test API

We can also use the website to test our API. Write the test code in json under the AWS Lambda function. Click `save` and `test`.

![image](https://gitlab.com/estrellas939/cargo_lambda_with_aws_api/-/raw/main/images/api-test-json.png?inline=false)

The result was displayed above.

![image](https://gitlab.com/estrellas939/cargo_lambda_with_aws_api/-/raw/main/images/api-test-result.png?inline=false)

` "body": "{\"message\":\"no such value due to same frequent\"}",` is the correct result we expected.
